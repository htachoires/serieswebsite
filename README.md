# Series Website

Development of a website allowing to browse, comment, rate a multitude of series from a database!

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes

### Prerequisites

You will need to install :

- [Symfony](https://symfony.com/download)


- [Composer](https://getcomposer.org/download/)

### Installing

First clone the repository and go to the dev branch

```
git clone https://gitlab-ce.iut.u-bordeaux.fr/htachoires/serieswebsite.git Series_Website
cd Series_Website
git checkout dev
```

Then install the dependencies with composer

```
composer install
```

Add on the .env.local file 

```yaml
DATABASE_URL=mysql://user:password@localhost/database?serverVersion=...
OMDB_API_KEY="your omdb key here"
YOUTUBE_API_KEY="your youtube api key here"
```

Finally, run the server

```
symfony serve
```


