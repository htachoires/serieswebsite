$(document).ready(function () {
    AOS.init({disable: 'mobile'});
});

const options = $('#select-season option').each(function () {
    $(this).val()
});

$('#select-season').change(function () {
    for (var i = 0; i < options.length; i++) {
        if (options[i].value === $('#select-season').val())
            $('.season-' + options[i].value).removeClass("d-none");
        else
            $('.season-' + options[i].value).addClass("d-none");
    }
});



