$(".btn-watch-episode").click(function () {
    let watchButton = this;
    let episodeId = watchButton.dataset.episodeId;
    startLoading(watchButton);

    let seasonButton = $("#season-watch-btn-" + watchButton.dataset.seasonId)[0];
    let nbEpisode = 0;
    $.get("/episode/watch/" + episodeId, function (data, status) {
        if (status === "success") {
            if (watchButton.childNodes[1].childNodes.item(1).name === "eye-off") {
                greyToGreen(watchButton);
                if(data.seasonWatched){
                    greyToGreen(seasonButton);
                }
            } else {
                greenToGrey(watchButton);
                greenToGrey(seasonButton);
            }
            console.log(data.nbEpisodeWatched);
            nbEpisode = data.nbEpisodeWatched;
            let seasonNumber = $("#season-watch-number-" + watchButton.dataset.seasonId);
            seasonNumber.text(nbEpisode);
        }
        stopLoading(watchButton);
    });
});

function startLoading(button) {
    let eye = button.childNodes[1];
    let loading = button.childNodes[3];
    loading.classList.replace('d-none', 'd');
    eye.classList.replace('d', 'd-none');
}

function stopLoading(button) {
    let eye = button.childNodes[1];
    let loading = button.childNodes[3];
    loading.classList.replace('d', 'd-none');
    eye.classList.replace('d-none', 'd');
}

function greenToGrey(button) {
    button.classList.replace("btn-success", "btn-secondary");
    let eye = button.childNodes[1].childNodes[1];
    eye.name = "eye-off";
}

function greyToGreen(button) {
    button.classList.replace("btn-secondary", "btn-success");
    let eye = button.childNodes[1].childNodes[1];
    eye.name = "eye";
}

$(".btn-watch-season").click(function () {
    let watchButton = this;
    startLoading(watchButton);
    let seasonId = watchButton.dataset.seasonId;
    let seasonNumber = $("#season-watch-number-" + watchButton.dataset.seasonId);
    $.get("/season/watchSeason/" + seasonId, function (data, status) {
        if (status === "success") {
            if (data.watched === true) {
                greyToGreen(watchButton);
            } else {
                greenToGrey(watchButton);
            }
            data.episode.forEach(episode => {
                let buttonEpisode = $('#episode-btn-' + episode)[0];
                if (data.watched === true) {
                    greyToGreen(buttonEpisode);
                } else {
                    greenToGrey(buttonEpisode);
                }
            });
            stopLoading(watchButton);
            seasonNumber.text(data.watched ? data.episode.length : 0);
        }

    });
});

