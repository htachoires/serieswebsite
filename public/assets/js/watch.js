$(".btn-watch-episode").click(function () {
    let watchButton = this;
    let episodeId = watchButton.dataset.episodeId;
    startLoading(watchButton);
    $.get("/episode/watch/" + episodeId, function (data, status) {
        if (status === "success") {
            if (watchButton.childNodes[1].childNodes[1].name === "eye-off") {
                greyToGreen(watchButton);
            } else {
                greenToGrey(watchButton);
            }
        }
        stopLoading(watchButton);
    });
});

function startLoading(button) {
    let eye = button.childNodes[1];
    let loading = button.childNodes[3];
    loading.classList.replace('d-none', 'd');
    eye.classList.replace('d', 'd-none');
}

function stopLoading(button) {
    let eye = button.childNodes[1];
    let loading = button.childNodes[3];
    loading.classList.replace('d', 'd-none');
    eye.classList.replace('d-none', 'd');
}

function greenToGrey(button) {
    button.classList.replace("btn-success", "btn-secondary");
    let eye = button.childNodes[1].childNodes[1];
    eye.name = "eye-off";
}

function greyToGreen(button) {
    button.classList.replace("btn-secondary", "btn-success");
    let eye = button.childNodes[1].childNodes[1];
    eye.name = "eye";
}