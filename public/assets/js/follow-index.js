$(".btn-follow").click(function () {
    let seriesId = this.dataset.seriesId;
    let followButton = this;
    startLoading(followButton);
    $.get("/series/follow/" + seriesId, function (data, status) {
        if (status === "success") {
            if (followButton.classList.contains('btn-info')) {
                followButton.childNodes[1].childNodes[1].name = "checkmark";
                followButton.classList.replace("btn-info", "btn-success")
            } else {
                followButton.childNodes[1].childNodes[1].name = "add";
                followButton.classList.replace("btn-success", "btn-info");
            }
            stopLoading(followButton);
        }
    });
});

function startLoading(button) {
    let eye = button.childNodes[1];
    let loading = button.childNodes[3];
    loading.classList.replace('d-none', 'd');
    eye.classList.replace('d', 'd-none');
}

function stopLoading(button) {
    let eye = button.childNodes[1];
    let loading = button.childNodes[3];
    loading.classList.replace('d', 'd-none');
    eye.classList.replace('d-none', 'd');
}