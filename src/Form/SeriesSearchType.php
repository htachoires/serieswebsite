<?php

namespace App\Form;

use App\Entity\Actor;
use App\Entity\Country;
use App\Entity\Genre;
use App\Entity\SeriesSearch;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SeriesSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod("GET")
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'multiple' => true,
                'required' => false,
                'attr'=> [
                    'class' => 'js-example-basic-single bar',
                ]
            ])
            ->add('genre', EntityType::class, [
                'class' => Genre::class,
                'multiple' => true,
                'required' => false,
                'attr'=> [
                    'class' => 'js-example-basic-single bar',
                ]
            ])
            ->add('actor', EntityType::class, [
                'class' => Actor::class,
                'multiple' => true,
                'required' => false,
                'attr'=> [
                    'class' => 'js-example-basic-single bar',
                ]
            ])
            ->add('titleSeries', SearchType::class, [
                'label' => 'series_title',
                'required' => false,
            ])
            ->add('sortType', ChoiceType::class, [
                'label' => 'sort_by',
                'choices' => [
                    'last-added' => 1,
                    'most_voted' => 2,
                    'top_rated' => 3,
                ],
                'required' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SeriesSearch::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
