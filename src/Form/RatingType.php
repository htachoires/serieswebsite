<?php

namespace App\Form;

use App\Entity\Rating;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RatingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('POST')
            ->add('comment', TextareaType::class, [
                'attr' => [
                    'placeholder' => '...',
                ],
                'required' => false
            ])
            ->add('value', ChoiceType::class, [
                'required' => true,
                'placeholder' => '...',
                'label' => false,
                'attr' => ['class' => 'mt-3'],
                'choices' => [
                    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                ]
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Rating::class,
        ]);
        $resolver->setRequired(array('series'));
    }
}
