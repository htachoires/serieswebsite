<?php

namespace App\Command;

use App\Entity\Series;
use App\Service\YoutubeTrailerGetter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class YoutubeTrailerCommand extends Command
{
    protected static $defaultName = 'app:update-trailers';

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var YoutubeTrailerGetter
     */
    private $youtubeTrailerGetter;

    /**
     * YoutubeTrailerCommand constructor.
     * @param EntityManagerInterface $manager
     * @param YoutubeTrailerGetter $youtubeTrailerGetter
     */
    public function __construct(EntityManagerInterface $manager, YoutubeTrailerGetter $youtubeTrailerGetter)
    {
        parent::__construct();
        $this->manager = $manager;
        $this->youtubeTrailerGetter = $youtubeTrailerGetter;
    }

    protected function configure()
    {
        $this->setDescription('Add the youtube trailer to the series in the database');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $series = $this->manager->getRepository(Series::class)->findAll();

        foreach ($series as $serie) {
            if(!$this->hasYoutubeTrailer($serie)) {

                $serie->setYoutubeTrailer($this->youtubeTrailerGetter->getYoutubeTrailerViaAPI($serie));
                $this->manager->persist($serie);

                $io->writeln("The youtube trailer of " . $serie->getTitle() . " has been updated");
            }
        }

        $this->manager->flush();

        $io->success('All the youtube trailers have been updated');
        return 0;
    }

    /**
     * Check if the series has a youtube trailer
     *
     * @param Series $series
     * @return bool true if the series has a trailer url, false either
     */
    private function hasYoutubeTrailer(Series $series) : bool
    {
        return !empty($series->getYoutubeTrailer());
    }
}
