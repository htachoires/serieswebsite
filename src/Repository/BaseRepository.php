<?php


namespace App\Repository;


use App\Entity\Country;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;


abstract class BaseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, $entityClass)
    {
        parent::__construct($registry, $entityClass);
    }

    public function findOneOrCreate(String $name)
    {
        $entity = $this->findOneBy(array("name" => $name));

        if (empty($entity))
        {
            $className = $this->getClassName();

            $entity = new $className;
            $entity->setName($name);
            $this->_em->persist($entity);
        }

        return $entity;
    }
}