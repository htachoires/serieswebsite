<?php

namespace App\Repository;

use App\Entity\Rating;
use App\Entity\Series;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Rating|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rating|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rating[]    findAll()
 * @method Rating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RatingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rating::class);
    }

    public function isSeriesRatedByUser(Series $series, ?User $user)
    {
        return count($this->findBy(array('series' => $series, 'user' => $user))) >= 1;
    }

    public function findLatestRating()
    {
        return $this->findBy([], ['id' => 'DESC'], 3);
    }
}
