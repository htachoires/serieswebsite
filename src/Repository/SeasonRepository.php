<?php

namespace App\Repository;

use App\Entity\Season;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Season|null find($id, $lockMode = null, $lockVersion = null)
 * @method Season|null findOneBy(array $criteria, array $orderBy = null)
 * @method Season[]    findAll()
 * @method Season[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeasonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Season::class);
    }

    public function isUserWatchedAllEpisodes(User $user, Season $season)
    {
        //not working
        $qb = $this->createQueryBuilder('season')
            ->join('season.episodes', 'episode')
            ->join('episode.user', 'user')
            ->andWhere(':user MEMBER OF episode.user')
            ->andWhere('episode MEMBER OF season.episodes')
            ->andWhere('season = :season')
            ->select('season', 'episode')
            ->setParameters(['season' => $season, ':user' => $user]);
        dd($qb->getQuery()->getResult());
        return;
    }

}