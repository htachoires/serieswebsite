<?php

namespace App\Repository;

use App\Entity\ExternalRating;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\Pagination\PaginationInterface;

/**
 * @method ExternalRating|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExternalRating|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExternalRating[]    findAll()
 * @method ExternalRating[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExternalRatingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExternalRating::class);
    }

    public function getRatingFromSeriesQuery(PaginationInterface $series_query) : array
    {
        $series_id = array_map(function ($object) { return $object->getId(); }, (array) $series_query->getItems());

        $query = $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.series IN (:series_id)')
            ->setParameter(':series_id', $series_id);

        return $query->getQuery()->getResult();
    }

}
