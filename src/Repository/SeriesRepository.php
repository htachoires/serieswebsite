<?php

namespace App\Repository;

use App\Entity\Episode;
use App\Entity\Series;
use App\Entity\SeriesSearch;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method Series|null find($id, $lockMode = null, $lockVersion = null)
 * @method Series|null findOneBy(array $criteria, array $orderBy = null)
 * @method Series[]    findAll()
 * @method Series[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Series::class);
    }

    /**
     * @param User $user
     * @return Series[]
     */
    public function findFollowedSeries(User $user): array
    {
        $qb = $this->createQueryBuilder('s')
            ->select('s, r')
            ->leftJoin('s.ratings', 'r', Join::WITH, 'r.user = :user')
            ->where(':user MEMBER OF s.user')
            ->setParameter('user', $user);

        return $qb->getQuery()->getResult();
    }

    /**
     * Return the lastest series added to the database
     * use a limit or -1 if all the
     *
     * @param int $limit
     * @return array
     */
    public function findBestRating(int $limit): array
    {
        $qb = $this->createQueryBuilder('s')
            ->select('s')
            ->join('s.externalRating', 'e')
            ->orderBy('e.value', 'DESC');

        if ($limit != -1) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Verify if the user follow the series
     *
     * @param Series $series
     * @param User $user
     * @return bool true if the user follow the series
     */
    public function isSeriesFollowedByUser(Series $series, User $user): bool
    {
        $qb = $this->createQueryBuilder('s')
            ->select('s')
            ->join('s.user', 'u')
            ->andWhere('u = :user')
            ->andWhere('s = :series')
            ->setParameters([':series' => $series, ':user' => $user]);

        return $qb->getQuery()->getResult() != null;
    }

    /**
     * Apply the different filters to the series in the database and return the query
     *
     * @param SeriesSearch $series_search the entity which contains all the filters value
     * @return Query the query with all the filter applied
     */
    public function findSeriesBySearchFilter(SeriesSearch $series_search): Query
    {
        $query = $this->createQueryBuilder('series')->select('series');

        $this->applyTitleFilter($series_search, $query);
        $this->applyCountryFilter($series_search, $query);
        $this->applyActorFilter($series_search, $query);
        $this->applyGenreFilter($series_search, $query);
        $this->applySortFilter($series_search, $query);

        return $query->getQuery();
    }

    /**
     * Return the lastest series added to the database
     * use a limit or -1 if all the
     *
     * @param int $limit
     * @return mixed
     */
    public function findLatestSeries(int $limit): array
    {
        $qb = $this->createQueryBuilder('s')
            ->select('s')
            ->orderBy('s.id', 'DESC')
            ->where('s.youtubeTrailer is not null')
            ->setMaxResults($limit);
        return $qb->getQuery()->getResult();
    }

    /**
     * Return the lastest series added to the database
     * use a limit or -1 if all the
     *
     * @param QueryBuilder $query
     * @return mixed
     */
    public function applyLatestSeries(QueryBuilder $query)
    {
        $query->select('series')
            ->orderBy('series.id', 'DESC');
        return $query;
    }

    /**
     * Return the id of the specified object
     *
     * @param $object
     * @return int
     */
    private function getObjectId($object): int
    {
        return $object->getId();
    }

    /**
     * Apply the country filter to the query
     *
     * @param SeriesSearch $series_search
     * @param QueryBuilder $query
     * @return void
     */
    private function applyCountryFilter(SeriesSearch $series_search, QueryBuilder $query): void
    {
        if ($series_search->getCountry() !== null && !$series_search->getCountry()->isEmpty()) {
            $countries_id = array_map(array($this, 'getObjectId'), $series_search->getCountry()->toArray());

            $query
                ->join('series.country', 'c')
                ->andWhere('c.id IN (:countries)')
                ->setParameter(':countries', $countries_id);
        }
    }

    /**
     * Apply the actor filter to the query
     *
     * @param SeriesSearch $series_search
     * @param QueryBuilder $query
     */
    private function applyActorFilter(SeriesSearch $series_search, QueryBuilder $query): void
    {
        if ($series_search->getActor() !== null && !$series_search->getActor()->isEmpty()) {
            $actors_id = array_map(array($this, 'getObjectId'), $series_search->getActor()->toArray());

            $query
                ->join('series.actor', 'a')
                ->andWhere('a.id IN (:actors)')
                ->setParameter(':actors', $actors_id);
        }
    }

    /**
     * Apply the genre filter to the query
     *
     * @param SeriesSearch $series_search
     * @param QueryBuilder $query
     */
    private function applyGenreFilter(SeriesSearch $series_search, QueryBuilder $query): void
    {
        if ($series_search->getGenre() !== null && !$series_search->getGenre()->isEmpty()) {
            $genre_id = array_map(array($this, 'getObjectId'), $series_search->getGenre()->toArray());

            $query
                ->join('series.genre', 'g')
                ->andWhere('g.id IN (:genre)')
                ->setParameter(':genre', $genre_id);
        }
    }

    /**
     * Apply the title filter to the query
     *
     * @param SeriesSearch $series_search
     * @param QueryBuilder $query
     */
    private function applyTitleFilter(SeriesSearch $series_search, QueryBuilder $query): void
    {
        if ($series_search->getTitleSeries()) {
            $query
                ->andWhere('series.title like :search_title')
                ->setParameter(':search_title', "%" . $series_search->getTitleSeries() . "%");
        }
    }

    /**
     * Apply one of the three filter : Top Rated, Most Voted, Most Viewed
     *
     * @param SeriesSearch $series_search
     * @param QueryBuilder $query
     */
    private function applySortFilter(SeriesSearch $series_search, QueryBuilder $query): void
    {
        if ($series_search->getSortType()) {
            switch ($series_search->getSortType()) {
                case 1:
                    $this->applyLatestSeries($query);
                    break;
                case 2:
                    $this->applyMostVotedFilter($query);
                    break;
                case 3:
                default:
                    $this->applyTopRatedFilter($query);
                    break;
            }
        }
    }

    private function applyTopRatedFilter(QueryBuilder $query)
    {
        $query
            ->leftJoin('series.externalRating', 'e')
            ->orderBy('e.value', 'DESC');
    }

    private function applyMostVotedFilter(QueryBuilder $query): void
    {
        $query
            ->leftJoin('series.externalRating', 'e')
            ->orderBy('e.votes', 'DESC');
    }
}