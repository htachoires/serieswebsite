<?php

namespace App\Repository;

use App\Entity\Episode;
use App\Entity\Season;
use App\Entity\Series;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\UserInterface;

/**
 * @method Episode|null find($id, $lockMode = null, $lockVersion = null)
 * @method Episode|null findOneBy(array $criteria, array $orderBy = null)
 * @method Episode[]    findAll()
 * @method Episode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EpisodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Episode::class);
    }

    public function findEpisodeWatchedBySeriesAndUser(Series $series, User $user)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->join('e.user', 'u')
            ->join('e.season', 'season')
            ->join('season.series', 'series')
            ->andWhere('u = :user')
            ->andWhere('series = :series')
            ->setParameters([':series' => $series, ':user' => $user]);
        return $qb->getQuery()->getResult();
    }

    public function nbEpisodeWatchByUserOfSeason(Season $season, User $user)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('count(e.id)')
            ->join('e.season', 's')
            ->andWhere(':user MEMBER OF e.user')
            ->andWhere('e.season = :season')
            ->setParameters([':season' => $season, ':user' => $user]);

        return $qb->getQuery()->getScalarResult();
    }
}
