<?php

namespace App\Controller;

use App\Entity\Season;
use App\Repository\EpisodeRepository;
use App\Repository\SeasonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/season")
 * @IsGranted("ROLE_USER")
 */
class SeasonController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    private $episodeRepository;

    private $seasonRepository;

    /**
     * EpisodeController constructor.
     * @param EntityManagerInterface $manager
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityManagerInterface $manager, TranslatorInterface $translator,
                                EpisodeRepository $episodeRepository, SeasonRepository $seasonRepository)
    {
        $this->manager = $manager;
        $this->translator = $translator;
        $this->episodeRepository = $episodeRepository;
        $this->seasonRepository = $seasonRepository;
    }

    /**
     * @Route("/watchSeason/{id}", name="watch_season", methods={"GET"})
     * @param Season $season the season that the user want to watch or unwatch
     * @return Response
     */
    public function watchSeason(Season $season): Response
    {
        $nbEpisode = $season->getEpisodes()->count();
        $seasonWatch = [];
        if ($this->episodeRepository->nbEpisodeWatchByUserOfSeason($season, $this->getUser())[0]['1'] == $nbEpisode) {
            $watched = false;
            foreach ($season->getEpisodes() as $episode) {
                $this->getUser()->removeEpisode($episode);
                $seasonWatch[] = $episode->getId();
            }
        } else {
            $watched = true;
            foreach ($season->getEpisodes() as $episode) {
                $this->getUser()->addEpisode($episode);
                $seasonWatch[] = $episode->getId();
            }
        }

        $this->manager->flush();

        $response = new Response();
        $response->setContent(json_encode([
            'episode' => $seasonWatch,
            'watched' => $watched
        ]));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
