<?php

namespace App\Controller;

use App\Entity\Episode;
use App\Entity\Series;
use App\Repository\EpisodeRepository;
use App\Repository\SeasonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/episode")
 * @IsGranted("ROLE_USER",message="you have to be loged in to access this content")
 */
class EpisodeController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    private $seasonRepository;

    private $episodeRepository;


    /**
     * EpisodeController constructor.
     * @param EntityManagerInterface $manager
     * @param TranslatorInterface $translator
     * @param SeasonRepository $seasonRepository
     * @param EpisodeRepository $episodeRepository
     */
    public function __construct(EntityManagerInterface $manager, TranslatorInterface $translator, SeasonRepository $seasonRepository,
                                EpisodeRepository $episodeRepository)
    {
        $this->manager = $manager;
        $this->seasonRepository = $seasonRepository;
        $this->translator = $translator;
        $this->episodeRepository = $episodeRepository;
    }

    /**
     * @Route("/watch/{id}", name="watch_episode", methods={"GET"})
     * @param Episode $episode
     * @return JsonResponse
     */
    public function watch(Episode $episode): Response
    {
        $series = $episode->getSeason()->getSeries();

        $this->userWatchOrUnwatchTheEpisode($series, $episode);
        $nbEpisodeWatched = $this->episodeRepository->nbEpisodeWatchByUserOfSeason($episode->getSeason(),$this->getUser());
        $seasonWatched = (int)$nbEpisodeWatched[0]['1'] === $episode->getSeason()->getEpisodes()->count();
        $response = new Response();
        $response->setContent(json_encode([
            'nbEpisodeWatched' => $nbEpisodeWatched[0]['1'],
            'seasonWatched' => $seasonWatched
        ]));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Add the episode to the user watched episode list if his haven't watched it already, removed it either
     *
     * @param Series $series the series who correspond to the episode
     * @param Episode $episode the episode to add or remove in the watched episode list of the user
     */
    private function userWatchOrUnwatchTheEpisode(Series $series, Episode $episode): void
    {
        if (!$this->getUser()->userHasWatchedTheEpisode($episode)) {
            $this->addEpisodeToTheUserWatchedList($episode);
        } else {
            $this->removeEpisodeFromTheUserWatchedList($episode);
        }
    }

    /**
     * Add the episode to the user episodes
     *
     * @param Episode $episode the episode added to the user episodes
     */
    private function addEpisodeToTheUserWatchedList(Episode $episode): void
    {
        $this->getUser()->addEpisode($episode);
        $this->manager->flush();
    }

    /**
     * Remove the episode from the user episodes
     *
     * @param Episode $episode the episode to remove from the user episodes list
     */
    private function removeEpisodeFromTheUserWatchedList(Episode $episode): void
    {
        $this->getUser()->removeEpisode($episode);
        $this->manager->flush();
    }
}
