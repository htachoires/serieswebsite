<?php

namespace App\Controller;

use App\Entity\Rating;
use App\Entity\Series;
use App\Form\RatingType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/rating")
 */
class RatingController extends AbstractController
{

    private $em;

    /**
     * RatingController constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * @Route("/", name="rating_index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN",message="Only an admin can access this content")
     */
    public function index(): Response
    {
        $ratings = $this->getDoctrine()
            ->getRepository(Rating::class)
            ->findAll();

        return $this->render('rating/index.html.twig', [
            'ratings' => $ratings,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="rating_edit", methods={"POST"})
     * @param Rating $rating the rating to edit
     * @param Request $request
     * @return RedirectResponse
     */
    public function edit(Rating $rating, Request $request)
    {
        try {
            $rating->setComment($request->request->get('update_rating')['comment']);
            $rating->setValue($request->request->get('update_rating')['value']);
            $this->em->flush();
            $this->addFlash('success', 'You rating has been updated');
            return $this->redirectToRoute('profile');
        } catch (\Exception $e) {
        }
        $this->addFlash('error', 'You rating hasn\'t been updated');
        return $this->redirectToRoute('profile');
    }

    /**
     * @Route("/new/{id}", name="rating_new", methods={"POST"})
     * @IsGranted("ROLE_USER")
     * @param Request $request
     * @param Series $series which series will be rate
     * @return Response
     */
    public function new(Request $request, Series $series): Response
    {
        $rating = new Rating();
        $form = $this->createForm(RatingType::class, $rating, array('series' => $series));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $rating->setUser($this->getUser())->setSeries($series);
            $this->em->persist($rating);
            $this->em->flush();
            $this->addFlash('success', 'Your comment has been shared with a rate of ' . $rating->getValue() . '/10 !');
            return $this->redirectToRoute('series_show', ['id' => $rating->getSeries()->getId()]);
        }
        return $this->redirectToRoute('series_show', [
            'rating' => $rating,
            'form' => $form->createView(),
            'id' => $series->getId()
        ]);
    }


    /**
     * @Route("/deleteComment/{id}", name="rating_delete_user", methods={"DELETE"})
     * @param Rating $rating the rating to be delete
     * @return Response
     */
    public function deleteRating(Rating $rating): Response
    {
        $series = $rating->getSeries();

        if ($this->getUser() == $rating->getUser() || $this->getUser()->getAdmin()) {
            $this->em->remove($rating);
            $this->em->flush();
            $this->addFlash('success', 'Your comment has been successfully removed !');
        } else {
            $this->addFlash('error', 'Cannot delete this comment !');
        }
        return $this->redirectToRoute('series_show', [
            'id' => $series->getId()
        ]);
    }

}
