<?php

namespace App\Controller;

use App\Entity\Series;
use App\Entity\SeriesSearch;
use App\Form\RatingType;
use App\Form\SeriesSearchType;
use App\Form\UpdateRatingType;
use App\Repository\EpisodeRepository;
use App\Repository\ExternalRatingRepository;
use App\Repository\RatingRepository;
use App\Repository\SeasonRepository;
use App\Repository\SeriesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/series")
 */
class SeriesController extends AbstractController
{
    private $seriesRepository;
    private $seasonRepository;
    private $ratingRepository;
    private $episodeRepository;
    private $externalRatingRepository;
    private $paginator;
    private $em;

    /**
     * @Route("", name="series_index", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $series_search = new SeriesSearch();
        $form = $this->createForm(SeriesSearchType::class, $series_search);
        $form->handleRequest($request);

        $series = $this->paginator->paginate($this->seriesRepository->findSeriesBySearchFilter($series_search)->getResult(),
            $request->query->getInt('page', 1), 5);

        return $this->render('series/index.html.twig', [
            'series' => $series,
            'seriesSearch' => true,
            'searchForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/poster/{id}", name="poster_show", methods={"GET"})
     * @param Series $series
     * @return Response
     */
    public function show_poster(Series $series): Response
    {
        $poster = stream_get_contents($series->getPoster());

        $response = new Response($poster);
        $response->headers->set('Content-Type', 'image/jpg');

        return $response;
    }


    /**
     * @Route("/{id}", name="series_show", methods={"GET"})
     * @param Series $series the series that we want to show
     * @param Request $request
     * @return Response
     */
    public function show(Series $series, Request $request): Response
    {
        $form = $this->createForm(RatingType::class, null, array('series' => $series));

        $ratings = $this->paginator->paginate($this->ratingRepository->findBy(array('series' => $series), array('id' => 'DESC')),
            $request->query->getInt('page', 1), 7);

        $episodesWatchedByUser = [];
        $isSeriesFollowedByUser = false;
        if ($this->getUser()) {
            $episodesWatchedByUser = $this->episodeRepository->findEpisodeWatchedBySeriesAndUser($series, $this->getUser());
            $isSeriesFollowedByUser = $this->seriesRepository->isSeriesFollowedByUser($series, $this->getUser());
        }
        $isRatedByUser = $this->ratingRepository->isSeriesRatedByUser($series, $this->getUser());
        return $this->render('series/show.html.twig', [
            'series' => $series,
            'ratings' => $ratings,
            'isRatedByUser' => $isRatedByUser,
            'update' => $this->createForm(UpdateRatingType::class),
            'formRating' => $form->createView(),
            'episodesWatchedByUser' => $episodesWatchedByUser,
            'isSeriesFollowedByUser' => $isSeriesFollowedByUser,
        ]);
    }

    /**
     * @Route("/follow/{id}", name="follow_series", methods={"GET"})
     * @IsGranted("ROLE_USER",message="need to login")
     *
     * @param Series $series the series that we want to follow or unfollow
     * @return JsonResponse
     */
    public function followSeries(Series $series): JsonResponse
    {
        if ($this->getUser()->getSeries()->contains($series)) {
            $this->getUser()->removeSeries($series);
        } else {
            $this->getUser()->addSeries($series);
        }
        $this->getDoctrine()->getManager()->flush();

        return $this->json("OK");
    }

    /**
     * @Route("/followProfile/{id}", name="profile_follow_series", methods={"POST"})
     * @IsGranted("ROLE_USER")
     *
     * @param Series $series the series that we want to follow or unfollow
     * @return JsonResponse
     */
    public function followSeriesProfile(Series $series): Response
    {
        if ($this->getUser()->getSeries()->contains($series)) {
            $this->getUser()->removeSeries($series);
        } else {
            $this->getUser()->addSeries($series);
        }
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('profile_my_series');
    }

    /**
     * SeriesController constructor.
     * @param SeriesRepository $seriesRepository
     * @param PaginatorInterface $paginator
     * @param SeasonRepository $seasonRepository
     * @param RatingRepository $ratingRepository
     * @param EpisodeRepository $episodeRepository
     * @param ExternalRatingRepository $externalRatingRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(SeriesRepository $seriesRepository, PaginatorInterface $paginator,
                                SeasonRepository $seasonRepository, RatingRepository $ratingRepository,
                                EpisodeRepository $episodeRepository, ExternalRatingRepository $externalRatingRepository,
                                EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        $this->seriesRepository = $seriesRepository;
        $this->seasonRepository = $seasonRepository;
        $this->ratingRepository = $ratingRepository;
        $this->episodeRepository = $episodeRepository;
        $this->externalRatingRepository = $externalRatingRepository;
        $this->paginator = $paginator;
    }
}
