<?php

namespace App\Controller;

use App\Form\RatingType;
use App\Form\UserFormType;
use App\Form\UpdateRatingType;
use App\Repository\EpisodeRepository;
use App\Repository\RatingRepository;
use App\Repository\SeriesRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/profile")
 */
class ProfileController extends AbstractController
{
    private $seriesRepository;
    private $userRepository;
    private $episodeRepository;
    private $paginator;
    private $ratingRepository;

    /**
     * ProfileController constructor.
     * @param SeriesRepository $seriesRepository
     * @param UserRepository $userRepository
     * @param PaginatorInterface $paginator
     * @param EpisodeRepository $episodeRepository
     * @param RatingRepository $ratingRepository
     */
    public function __construct(SeriesRepository $seriesRepository, UserRepository $userRepository,
                                PaginatorInterface $paginator, EpisodeRepository $episodeRepository,
                                RatingRepository $ratingRepository)
    {
        $this->seriesRepository = $seriesRepository;
        $this->userRepository = $userRepository;
        $this->episodeRepository = $episodeRepository;
        $this->paginator = $paginator;
        $this->ratingRepository = $ratingRepository;
    }

    /**
     * @Route("/", name="profile")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function index(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(UserFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));

            $manager->persist($user);
            $manager->flush();

            $this->addFlash('success', 'Your profile have been successfully updated !');
        }

        return $this->render('profile/index.html.twig', [
            'controller_name' => 'ProfileController',
            'profile' => true,
            'myProfile' => true,
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/myComments", name="profile_my_comments")
     * @param Request $request
     * @return Response
     */
    public function ratingProfile(Request $request): Response
    {
        $userRatings =
        $userRatings = $this->paginator->paginate($this->ratingRepository->findBy(['user' => $this->getUser()], ['id' => 'ASC']),
            $request->query->getInt('page', 1), 4);

        return $this->render('profile/my_comments.html.twig', [
            'profile' => true,
            'myComments' => true,
            'userRatings' => $userRatings,
            'update' => $this->createForm(UpdateRatingType::class),
        ]);
    }

    /**
     * @Route("/mySeries", name="profile_my_series")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function followedSeries(Request $request)
    {
        $series = $this->paginator->paginate($this->seriesRepository->findFollowedSeries($this->getUser()), $request->query->getInt('page', 1), 1);

        if ($series->getTotalItemCount() == 0) {
            $this->addFlash('error', 'You don\'t follow any series !');
            return $this->redirectToRoute('profile');
        }

        $form = $this->createForm(RatingType::class, null, array('series' => $series->getItems()[0]));
        $episodesWatchedByUser = $this->episodeRepository->findEpisodeWatchedBySeriesAndUser($series->getItems()[0], $this->getUser());
        return $this->render('profile/followed_series.html.twig', [
            'controller_name' => 'ProfileController',
            'profile' => true,
            'Allseries' => $series,
            'formRating' => $form->createView(),
            'episodesWatchedByUser' => $episodesWatchedByUser,
            'mySeries'=>true,
            'update' => $this->createForm(UpdateRatingType::class)
        ]);
    }
}
