<?php

namespace App\Controller;

use App\Entity\Episode;
use App\Entity\ExternalRating;
use App\Entity\Season;
use App\Entity\Series;
use App\Repository\ActorRepository;
use App\Repository\CountryRepository;
use App\Repository\ExternalRatingSourceRepository;
use App\Repository\GenreRepository;
use App\Repository\SeriesRepository;
use App\Service\OmdbApiWrapper;
use App\Service\YoutubeTrailerGetter;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class OmdbController extends AbstractController
{
    /**profile
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var CountryRepository
     */
    private $countryRepository;
    /**
     * @var String
     */
    private $error_message;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var YoutubeTrailerGetter
     */
    private $youtubeTrailerGetter;
    /**
     * @var ActorRepository
     */
    private $actorRepository;
    /**
     * @var GenreRepository
     */
    private $genreRepository;
    /**
     * @var ExternalRatingSourceRepository
     */
    private $externalRatingSourceRepository;
    /**
     * @var SeriesRepository
     */
    private $seriesRepository;

    /**
     * OmdbController constructor.
     *
     * @param EntityManagerInterface $manager
     * @param LoggerInterface $logger
     * @param CountryRepository $countryRepository
     * @param ActorRepository $actorRepository
     * @param GenreRepository $genreRepository
     * @param ExternalRatingSourceRepository $externalRatingSourceRepository
     * @param TranslatorInterface $translator
     * @param YoutubeTrailerGetter $youtubeTrailerGetter
     * @param SeriesRepository $seriesRepository
     */
    public function __construct(EntityManagerInterface $manager, LoggerInterface $logger,
                                CountryRepository $countryRepository, ActorRepository $actorRepository,
                                GenreRepository $genreRepository, ExternalRatingSourceRepository $externalRatingSourceRepository,
                                TranslatorInterface $translator, YoutubeTrailerGetter $youtubeTrailerGetter, SeriesRepository $seriesRepository)
    {
        $this->translator = $translator;
        $this->manager = $manager;
        $this->logger = $logger;
        $this->countryRepository = $countryRepository;

        $this->youtubeTrailerGetter = $youtubeTrailerGetter;
        $this->actorRepository = $actorRepository;
        $this->genreRepository = $genreRepository;
        $this->externalRatingSourceRepository = $externalRatingSourceRepository;
        $this->seriesRepository = $seriesRepository;
    }

    /**
     * @Route("/omdb/", name="omdb", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN", message="Only an administrator can access this content !")
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function index(Request $request)
    {
        $search = $request->query->get('search');
        $validate = $request->query->get('persist');
        $already_in_database = false;
        $series = null;
        $poster_url = null;
        $imdb_rating = null;

        if (!empty($search)) {
            if (OmdbApiWrapper::isImdbId($search)) {
                $series = $this->seriesRepository->findOneBy(array('imdb' => $search));
            } else {
                $series = $this->seriesRepository->findOneBy(array('title' => $search));
            }
            if (empty($series)) {

                $omdb = new OmdbApiWrapper($search);

                if (empty($omdb->getApiError())) {
                    if ($omdb->getType() !== "series") {
                        $this->addFlash('error', $this->translator->trans('flash_error_type_omdb', ['%type%' => $omdb->getType()]));
                    } else {
                        $series = $this->copyDataToSeriesEntity($omdb);
                        $is_in_database = $this->seriesRepository->findBy(['imdb' => $series->getImdb()]);
                        if ($is_in_database != null) {
                            $series = $is_in_database[0];
                            $this->addFlash('error', $this->translator->trans('flash_already_in_db', ['%series%' => $series->getTitle()]));
                            $already_in_database = true;
                        } elseif ($validate) {
                            $this->processYoutubeTrailer($series);
                            $this->manager->persist($series);
                            $this->manager->flush();
                            $this->addFlash('success', $this->translator->trans('flash_add_series_omdb', ['%series%' => $series->getTitle()]));
                            $already_in_database = true;
                        }

                        $poster_url = $omdb->getPosterUrl();
                        $imdb_rating = $omdb->getImdbRating();
                    }
                } else {
                    $this->addFlash('error', $omdb->getApiError());
                }
            } else {
                $already_in_database = true;
                $this->addFlash('error', $this->translator->trans('flash_already_in_db', ['%series%' => $series->getTitle()]));
                $poster_url = "data:image/png;base64, " . $series->getPosterImage();
            }
        }

        return $this->render('omdb_admin/index.html.twig', [
            'series' => $series,
            'poster_url' => $poster_url,
            'imdb_rating' => $imdb_rating,
            'omdb' => true,
            'already_in_database' => $already_in_database,
        ]);
    }

    private function copyDataToSeriesEntity(OmdbApiWrapper $omdb): Series
    {
        $series = new Series();

        $series->setTitle($omdb->getTitle());
        $series->setImdb($omdb->getImdb());
        $series->setYearStart($omdb->getYearStart());
        $series->setYearEnd($omdb->getYearEnd());
        $series->setDirector($omdb->getDirector());
        $series->setPlot($omdb->getPlot());

        $this->processPoster($series, $omdb->getPosterUrl());
        $this->processActors($series, $omdb->getActors());
        $this->processCountry($series, $omdb->getCountry());
        $this->processGenre($series, $omdb->getGenre());
        $this->processSeasonsAndEpisodes($series, $omdb->getSeasons());
        $this->processExternalRating($series, $omdb->getRatings(), $omdb->getImdbVotes());

        return $series;
    }

    /**
     * Parse the actor to add them to the series
     *
     * @param Series $series the series who will contains the actors
     * @param array $actors
     */
    private function processActors(Series $series, array $actors): void
    {
        foreach ($actors as $actor_name) {
            $series->addActor($this->actorRepository->findOneOrCreate($actor_name));
        }
    }

    /**
     * Add the country of the series
     *
     * @param Series $series the series where the country will be added
     * @param String $country_name the name of the country
     */
    private function processCountry(Series $series, String $country_name): void
    {
        $series->addCountry($this->countryRepository->findOneOrCreate($country_name));
    }

    /**
     * Add the genres to the series
     *
     * @param Series $series the series where the genres will be added
     * @param array $genre
     */
    private function processGenre(Series $series, array $genre): void
    {
        foreach ($genre as $genre_name) {
            $series->addGenre($this->genreRepository->findOneOrCreate($genre_name));
        }
    }

    /**
     * Add the poster data to the series
     *
     * @param Series $series
     * @param String $poster_url the url of the poster
     * @return void
     */
    private function processPoster(Series $series, String $poster_url): void
    {
        if ($poster_url != "N/A") {
            $series->setPoster(file_get_contents($poster_url));
        }
    }


    /**
     * Add the ratings of the ranting in the database
     *
     * @param Series $series the rated series
     * @param array $ratings an array of all the ratings made by the different source
     */
    private function processExternalRating(Series $series, array $ratings, int $imdbVotes): void
    {
        foreach ($ratings as $rating) {
            $source = $this->externalRatingSourceRepository->findOneOrCreate($rating["Source"]);

            $external_rating = new ExternalRating();
            $external_rating->setValue($rating["Value"]);
            $external_rating->setSource($source);

            if ($source->getId() == 1) {
                $external_rating->setVotes($imdbVotes);
            }

            $series->addExternalRating($external_rating);
            $this->manager->persist($external_rating);
        }
    }

    private function processYoutubeTrailer(Series $series)
    {
        if ($this->youtubeTrailerGetter->isApiKeyDefined()) {
            $series->setYoutubeTrailer($this->youtubeTrailerGetter->getYoutubeTrailerViaAPI($series->getTitle()));
        }
    }

    /**
     * Add the seasons and the episodes to the series
     *
     * @param Series $series
     * @param array $seasons
     * @throws Exception
     */
    private function processSeasonsAndEpisodes(Series $series, array $seasons): void
    {
        foreach ($seasons as $season_array) {
            if ($season_array["Response"] === "True") {
                $season = new Season();
                $season->setNumber($season_array["Season"]);

                foreach ($season_array["Episodes"] as $episode_array) {
                    $episode = new Episode();
                    $episode->setTitle($episode_array["Title"]);
                    $this->parseEpisodeDate($episode, $episode_array["Released"]);
                    $episode->setNumber($episode_array["Episode"]);
                    $episode->setImdb($episode_array["imdbID"]);
                    $episode->setImdbrating((float)$episode_array["imdbRating"]);
                    $episode->setSeason($season);

                    $this->manager->persist($episode);
                    $season->addEpisode($episode);
                }

                $series->addSeason($season);
                $this->manager->persist($season);
            }
        }
    }

    /**
     * Parse the date of an episode to create an DateTime object from a string
     *
     * @param Episode $episode
     * @param String $date_string the string of the date in the format 'year-month-day'
     * @throws Exception
     */
    private function parseEpisodeDate(Episode $episode, String $date_string): void
    {
        $date = new DateTime();
        $date->createFromFormat('y-m-d', $date_string);

        $episode->setDate($date);
    }
}
