<?php

namespace App\Controller;

use App\Repository\GenreRepository;
use App\Repository\RatingRepository;
use App\Repository\SeriesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private $genreRepository;
    private $seriesRepository;
    private $ratingRepository;

    /**
     * HomeController constructor.
     * @param GenreRepository $genreRepository
     * @param SeriesRepository $seriesRepository
     * @param RatingRepository $ratingRepository
     */
    public function __construct(GenreRepository $genreRepository, SeriesRepository $seriesRepository, RatingRepository $ratingRepository)
    {
        $this->genreRepository = $genreRepository;
        $this->seriesRepository = $seriesRepository;
        $this->ratingRepository = $ratingRepository;
    }


    /**
     * @Route("/", name="home")
     */
    public function index()
    {

        $genres = $this->genreRepository->findOrderByName(1);
        $latestSeries = $this->seriesRepository->findLatestSeries(10);
        $series = $this->seriesRepository->findBestRating(5);
        $latestRating = $this->ratingRepository->findLatestRating();

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'home' => true,
            'genres' => $genres,
            'series' => $series,
            'latestSeries' => $latestSeries,
            'latestRating' => $latestRating,
        ]);
    }

    /**
     * @Route("/about", name="aboutUS")
     */
    public function aboutUs()
    {
        return $this->render('home/about.html.twig', [
            'controller_name' => 'HomeController',
            'about' => true
        ]);
    }
}
