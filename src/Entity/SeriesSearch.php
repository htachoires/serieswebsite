<?php


namespace App\Entity;


use Doctrine\Common\Collections\Collection;

class SeriesSearch
{
    /**
     * @var String|null
     */
    private $titleSeries;

    /**
     * @var Collection|null
     */
    private $country;

    /**
     * @var Collection|null
     */
    private $genre;

    /**
     * @var Collection|null
     */
    private $actor;

    /**
     * @var int
     */
    private $sortType = 1;

    /**
     * @return Collection|null
     */
    public function getCountry(): ?Collection
    {
        return $this->country;
    }

    /**
     * @param Collection|null $country
     * @return SeriesSearch
     */
    public function setCountry(?Collection $country): SeriesSearch
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param Collection|null $genre
     * @return SeriesSearch
     */
    public function setGenre(?Collection $genre): SeriesSearch
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getActor(): ?Collection
    {
        return $this->actor;
    }

    /**
     * @param Collection|null $actor
     * @return SeriesSearch
     */
    public function setActor(?Collection $actor): SeriesSearch
    {
        $this->actor = $actor;
        return $this;
    }

    /**
     * @return String|null
     */
    public function getTitleSeries(): ?String
    {
        return $this->titleSeries;
    }

    /**
     * @param String|null $title
     * @return SeriesSearch
     */
    public function setTitleSeries(?String $title): SeriesSearch
    {
        $this->titleSeries = $title;
        return $this;
    }

    /**
     * @return int
     */
    public function getSortType(): int
    {
        return $this->sortType;
    }

    /**
     * @param int $sortType
     * @return SeriesSearch
     */
    public function setSortType(int $sortType): SeriesSearch
    {
        $this->sortType = $sortType;
        return $this;
    }

}