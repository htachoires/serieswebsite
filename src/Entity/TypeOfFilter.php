<?php


namespace App\Entity;


abstract class TypeOfFilter
{
    const MOST_VIEWED = "view";
    const MOST_COMMENTED = "comment";
    const MOST_RATED = "rate";
}