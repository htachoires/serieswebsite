<?php


namespace App\Service;


use GuzzleHttp\Client;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Session\Session;

class OmdbApiWrapper
{
    /**
     * @var String
     */
    private $OMDB_API_KEY;

    /**
     * @var String
     */
    private $title;

    /**
     * @var String
     */
    private $imdb;

    /**
     * @var String
     */
    private $yearStart;

    /**
     * @var String|null
     */
    private $yearEnd;

    /**
     * @var String
     */
    private $director;

    /**
     * @var array
     */
    private $genre;

    /**
     * @var array
     */
    private $actors;

    /**
     * @var String
     */
    private $plot;

    /**
     * @var String
     */
    private $country;

    /**
     * @var String
     */
    private $awards;

    /**
     * @var String
     */
    private $posterUrl;

    /**
     * @var array
     */
    private $ratings;

    /**
     * @var float
     */
    private $imdbRating;

    /**
     * @var int
     */
    private $imdbVotes;

    /**
     * @var String
     */
    private $type;

    /**
     * @var array
     */
    private $seasons;

    /**
     * @var int
     */
    private $totalSeasons;

    /**
     * @var String
     */
    private $apiError;

    public function __construct(String $search)
    {
        $dotenv = new Dotenv();
        $dotenv->load(__DIR__ . '/../../.env.local');
        $this->OMDB_API_KEY = $_ENV['OMDB_API_KEY'];

        $this->genre = array();
        $this->actors = array();
        $this->ratings = array();
        $this->seasons = array();

        $this->initialize($search);
    }

    /**
     * Initialize the class fill all the attributes
     *
     * @param String $search
     */
    private function initialize(String $search)
    {
        if ($this->isImdbId($search)) {
            $parameters = "i=" . $search;
            $data = $this->makeRequest($parameters);
        } else {
            $parameters = "t=" . $search;
            $data = $this->makeRequest($parameters);
        }

        if ($data["Response"] === "True") {
            $this->setData($data);
        } else {
            $this->apiError = $data["Error"];
        }
    }

    /**
     * Check if the string have an imdb Id format
     *
     * @param String $imdbId
     * @return bool true if the string is an imdb id
     */
    public static function isImdbId(String $imdbId): bool
    {
        $pattern = '/^tt[0-9]{7,8}$/';

        return preg_match($pattern, $imdbId);
    }

    /**
     * Set the data of the series using the data senbt by the API
     *
     * @param array $data the data sent by the OMDB API
     */
    private function setData(array $data) : void
    {
        $this->setTitle($data["Title"]);
        $this->setImdb($data["imdbID"]);
        $this->setPlot($data["Plot"]);
        $this->setDirector($data["Director"]);
        $this->setCountry($data["Country"]);
        $this->setAwards($data["Awards"]);
        $this->setPosterUrl($data["Poster"]);
        $this->setRatings($data["Ratings"]);
        $this->setImdbRating((float) $data["imdbRating"]);
        $this->setType($data["Type"]);

        if ($this->type === "series") {
            $this->setTotalSeasons((int)$data["totalSeasons"]);
        }

        $this->parseYears($data["Year"]);
        $this->parseGenre($data["Genre"]);
        $this->parseActors($data["Actors"]);
        $this->parseImdbVotes($data["imdbVotes"]);

        $this->retrieveSeasons();

        //dd($this);
    }

    /**
     * Make the request to the omdb API with different parameters
     *
     * @param String $parameters the parameters used to make the request
     * @return mixed an array who contains the data on the series
     */
    private function makeRequest(String $parameters)
    {
        $client = new Client();
        $response = $client->request('GET', 'http://www.omdbapi.com/?apikey=' . $this->OMDB_API_KEY . "&plot=full&type=series&" . $parameters);

        if ($response->getStatusCode() === 200) {
            return json_decode($response->getBody()->getContents(), true);
        }
    }

    /**
     * Parse the YearStart and YearEnd attributes
     *
     * @param String $year a string which contains the series years
     */
    private function parseYears(String $year) : void
    {
        $year_parse_invalid_character = str_replace('–', ' ', $year);
        $year_array = explode(' ', $year_parse_invalid_character);

        $this->setYearStart($year_array[0]);

        if (!empty($year_array[1])) {
            $this->setYearEnd($year_array[1]);
        }
    }


    /**
     * Parse the genre of the series into the genre attribute
     *
     * @param String $genre_string the string who contains all the genre separated by commas
     */
    private function parseGenre(String $genre_string) : void
    {
        $genre_array = explode(',', $genre_string);
        $trimmed_genre = array_map('trim', $genre_array);

        foreach ($trimmed_genre as $genre_name) {
            array_push($this->genre, $genre_name);
        }
    }


    /**
     * Parse the actors of the series into an array
     *
     * @param String $actors_string the string who contains all the actors of the series separated by commas
     */
    private function parseActors(String $actors_string) : void
    {
        $actors_array = explode(',', $actors_string);
        $trimmed_actors = array_map('trim', $actors_array);

        foreach ($trimmed_actors as $actor_name) {
            array_push($this->actors, $actor_name);
        }
    }

    /**
     * Parse the number of imdb votes to convert it in an integer
     *
     * @param String $imdbVotes the string of the number of imdb votes
     */
    private function parseImdbVotes(String $imdbVotes) : void
    {
        $imdbVotes = intval(str_replace(',', '', $imdbVotes));
        $this->setImdbVotes($imdbVotes);
    }

    /**
     * Retrieve the seasons and the episodes of the series
     */
    private function retrieveSeasons() : void
    {
        if ($this->totalSeasons !== null && $this->totalSeasons != 0) {
            for ($i = 1; $i <= $this->totalSeasons; $i++) {
                $parameters = "i=" . $this->imdb . "&Season=" . $i;
                $season = $this->makeRequest($parameters);

                array_push($this->seasons, $season);
            }
        }
    }

    /**
     * @return String
     */
    public function getTitle(): String
    {
        return $this->title;
    }

    /**
     * @param String $title
     */
    public function setTitle(String $title): void
    {
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function getGenre(): array
    {
        return $this->genre;
    }

    /**
     * @return array
     */
    public function getActors(): array
    {
        return $this->actors;
    }

    /**
     * @return String
     */
    public function getPlot(): String
    {
        return $this->plot;
    }

    /**
     * @param String $plot
     */
    public function setPlot(String $plot): void
    {
        $this->plot = $plot;
    }

    /**
     * @return array
     */
    public function getCountry(): String
    {
        return $this->country;
    }

    /**
     * @param String $country
     */
    public function setCountry(String $country): void
    {
        $this->country = $country;
    }

    /**
     * @return String
     */
    public function getAwards(): String
    {
        return $this->awards;
    }

    /**
     * @param String $awards
     */
    public function setAwards(String $awards): void
    {
        $this->awards = $awards;
    }

    /**
     * @return String
     */
    public function getPosterUrl(): String
    {
        return $this->posterUrl;
    }

    /**
     * @param String $posterUrl
     */
    public function setPosterUrl(String $posterUrl): void
    {
        $this->posterUrl = $posterUrl;
    }

    /**
     * @return array
     */
    public function getRatings(): array
    {
        return $this->ratings;
    }

    /**
     * @param array $ratings
     */
    public function setRatings(array $ratings): void
    {
        $this->ratings = $ratings;
    }

    /**
     * @return float
     */
    public function getImdbRating(): float
    {
        return $this->imdbRating;
    }

    /**
     * @param float $imdbRating
     */
    public function setImdbRating(float $imdbRating): void
    {
        $this->imdbRating = $imdbRating;
    }

    /**
     * @return int
     */
    public function getImdbVotes(): int
    {
        return $this->imdbVotes;
    }

    /**
     * @param int $imdbVotes
     */
    public function setImdbVotes(int $imdbVotes): void
    {
        $this->imdbVotes = $imdbVotes;
    }

    /**
     * @return String
     */
    public function getType(): ?String
    {
        return $this->type;
    }

    /**
     * @param String $type
     */
    public function setType(String $type): void
    {
        $this->type = $type;
    }

    /**
     * @param int $totalSeasons
     */
    public function setTotalSeasons(int $totalSeasons): void
    {
        $this->totalSeasons = $totalSeasons;
    }

    /**
     * @return String
     */
    public function getYearStart(): String
    {
        return $this->yearStart;
    }

    /**
     * @param String $yearStart
     */
    public function setYearStart(String $yearStart): void
    {
        $this->yearStart = $yearStart;
    }

    /**
     * @return String|null
     */
    public function getYearEnd(): ?String
    {
        return $this->yearEnd;
    }

    /**
     * @param String $yearEnd
     */
    public function setYearEnd(String $yearEnd): void
    {
        $this->yearEnd = $yearEnd;
    }

    /**
     * @return String
     */
    public function getImdb(): String
    {
        return $this->imdb;
    }

    /**
     * @param String $imdb
     */
    public function setImdb(String $imdb): void
    {
        $this->imdb = $imdb;
    }

    /**
     * @return String
     */
    public function getDirector(): String
    {
        return $this->director;
    }

    /**
     * @param String $director
     */
    public function setDirector(String $director): void
    {
        $this->director = $director;
    }

    /**
     * @return array
     */
    public function getSeasons(): array
    {
        return $this->seasons;
    }

    /**
     * @return String|null
     */
    public function getApiError(): ?String
    {
        return $this->apiError;
    }
}