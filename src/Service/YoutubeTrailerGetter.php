<?php


namespace App\Service;


use App\Entity\Series;
use Google_Client;
use Google_Service_YouTube;
use Symfony\Component\Dotenv\Dotenv;

class YoutubeTrailerGetter
{
    private $YOUTUBE_API_KEY = null;

    public function __construct()
    {
        $dotenv = new Dotenv();
        $dotenv->load(__DIR__ . '/../../.env.local');

        try {
            $this->YOUTUBE_API_KEY = $_ENV['YOUTUBE_API_KEY'];
        } catch (\ErrorException $exception) {
            $this->YOUTUBE_API_KEY = null;
        }
    }

    /**
     * Set the youtube trailer of a series using the youtube API
     *
     * @param String $seriesName the name of the series
     * @return String a youtube url of the trailer
     */
    public function getYoutubeTrailerViaAPI(String $seriesName) : String
    {
        $client = new Google_Client();

        $client->setApplicationName('Youtube Trailer Retrieve');
        $client->setDeveloperKey($this->YOUTUBE_API_KEY);

        $service = new Google_Service_YouTube($client);

        $queryParams = [
            'maxResults' => 1,
            'q' => $seriesName . ' trailer'
        ];

        $response = $service->search->listSearch('snippet', $queryParams);

        $videoId = $response['items'][0]['id']['videoId'];

        return "https://www.youtube.com/watch?v=" . $videoId;
    }

    public function isApiKeyDefined() : bool
    {
        return !empty($this->YOUTUBE_API_KEY);
    }
}